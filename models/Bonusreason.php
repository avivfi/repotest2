<?php

namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;
	
/**
 * This is the model class for table "bonusreason".
 *
 * @property integer $id
 * @property string $name
 */
class Bonusreason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonusreason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	
	public static function getBonusreason()
	{
		$allBonusreason = self::find()->all();
		$allBonusreasonArray = ArrayHelper::
					map($allBonusreason, 'id', 'name');
		return $allBonusreasonArray;						
	}
}
